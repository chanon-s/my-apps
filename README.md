# My Apps

<img src="https://code.visualstudio.com/favicon.ico" width="48" />

[Visual Studio Code](https://code.visualstudio.com/)

<img src="https://visualstudio.microsoft.com/wp-content/uploads/2021/10/Product-Icon.svg" width="48" />

[Visual Studio](https://visualstudio.microsoft.com/)

<img src="https://www.docker.com/favicon.ico" width="48"/>

[Docker Desktop](https://www.docker.com/)

<img src="https://www.nicepng.com/png/full/85-851058_anaconda-icon-anaconda-python-icon.png" width="48"/>

[Anaconda](https://www.anaconda.com/)

<img src="https://wac-cdn.atlassian.com/assets/img/favicons/sourcetree/favicon.ico" width="48">

[Sourcetree](https://www.sourcetreeapp.com/)

<img src="https://git-fork.com/images/logo.png" width="48">

[Fork](https://git-fork.com/)
